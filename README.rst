Dokumentation
=============

Aufgabenstellung
----------------

* HTML-Formular, dass Daten an eine PHP-Datei übermittelt und Werte in eine Datenbank einträgt (INSERT)
* Der Benutzer soll mehrere Werte eintragen können
* Aus den Benutzerangaben soll mindestens ein weiterer Wert berechnet werden
* In Abhängigkeit des berechneten Wertes soll ein weiterer Wert festgelegt werden
* Der Benutzer soll ein Feedback bekommen über die eingetragenen, berechneten und festgelegten Werte bekommen
* Der Benutzer soll zusätzlich ein Feedback über Datenbankeinträge bekommen, die einen entsprechenden Tabelleneintrag enthalten (SELECT ... WHERE)


Umgebung
--------

Wir gehen hier von einem openSUSE (Tumbleweed) Linux aus.
Ich bin Freund `freier <https://de.wikipedia.org/wiki/Freie-Software-Bewegung/>`_ Software und leider auch ein ein armer Schlucker.
Bis auf die Installation von Docker sollten alle Schritte in jedem beliebigem Linux nachzuvollziehen sein.
Was im Endeffekt eh keine Rolle spielt, mindestens bis Abgabetermin wird das Projekt auf `Uberspace.de <https://uberspace.de/>`_ gehostet sein.
In meinen Hemdsärmligen 20 Jahren Linux und einem Back-to-the-Roots Mindset habe ich mich nie mit "Klickibunti" anfreunden können.
Ich bitte deswegen meinen "FiSi" Ansatz zu entschuldigen.



Docker
------

Da ich für ein anderes Projekt lernen muss mit Docker umzugehen und die Installation von Xampp nicht so glatt lief, lag es nah das PHP Projekt in einem Container zu "entwickeln".

.. code-block:: shell-session

    # repo nach ~/p_weather clonen
    git clone git@codeberg.org:denn_moe/p_weather.git ~/p_weather
    
    # benötigte Pakete installieren
    sudo zypper install docker docker-compose docker-compose-switch

    # Benutzer zur Docker Gruppe hinzufügen
    sudo usermod -G docker -a $USER

    # Docker Deamon aktivieren
    sudo systemctl enable docker

    # Neustart durchführen
    sudo reboot

    # Docker Image runterladen
    docker pull tomsik68/xampp

    # Container initalisieren 
    docker run --name php-Xampp -p 42022:22 -p 42080:80 -d -v ~/p_weather:/www tomsik68/xampp:8

    # Container starten/stoppen/löschen
    docker start php-Xampp 
    docker stop php-Xampp
    docker rm php-Xampp


Solange der Container läuft erreichen wir über `http://localhost:42080/` das Xampp Interface und `http://localhost:42080/www` öffnet das Projekt.

Damit die Wetterdaten aus dem Internet geladen werden können, muss im Container der DNS-Server eingestellt werden.

.. code-block:: console

    # per SSH mit Container verbinden
    ssh root@localhost -p 42022

    # Passwort
    root

    # /etc/default/docker anpassen
    echo 'DOCKER_OPTS="--dns 208.67.222.222 --dns 208.67.220.220"' > /etc/default/docker

    # SSH Konsole verlassen
    exit

    # Docker Deamon neustarten
    sudo service docker restart


Wetterdaten
-----------

Die Daten des `DWD <https://opendata.dwd.de/>`_ sind frei verfügbar.
Für eine deutsche Behörde typisch sind die Bytes ziemlich klobig, die da raus purzeln.
Glücklicherweise bietet `https://open-meteo.com/en/docs/dwd-api>`_ eine einfache Möglichkeit aktuelle Wetterdaten per JSON abzugreifen.
Um an die Temperatur einer Stadt zu erhalten, brauchen wir allerdings die Geographischen Koordinaten.

Gibt es eine Liste mit den Koordinaten aller deutschen Städte samt Postleitzahlen?

Nach kurzer Befragung der Suchmaschine meines geringsten Misstrauens, stieß ich auf `OpenGeoDB <http://opengeodb.org/wiki/OpenGeoDB>`_.
Letzte aktuallisierung 2006.
Hoffen wir mal, dass in Deutschland nicht so oft neue Städte dazukommen...
Pech gehabt, 404.
Freundlicherweise hat ein "unknown Stranger" die Daten auf ein `GitLab repo <https://gitlab.com/GriepMarco/opengeodb-radius-api/-/tree/main>`_ hochgeladen.

Mit etwas JSON und Datenbank Vodoo können wir jetzt anhand von Postleitzahl bzw. Stadtname auf die aktuellen Wetterdaten zugreifen.


Umsetzung
---------

Anfänglich hatte ich geplant, Durchschnittswerte von den Wetterdaten zu berechnen und diese beispielsweise stündlich, vom Homeserver aus, zu aktualisieren.
Nach längerem rumspielen musste ich dann aber feststellen, dass ich zum einen mit Datenbanken zu wenig umgehen kann und zum anderen meine Schwierigkeiten mit PHP habe.
Ich schaffe es z.B. nicht die Daten des SQL-Servers in separate Variablen zu speichern.

Somit hoffen wir jetzt mal, dass die Dozentin die Anforderungen nicht ganz so eng nimmt, da hier nichts Neues berechnet wird.

Die PLZ.Tab Datei wurde von phpmyadmin anstandslos als CSV entgegen genommen.
Einzig zu beachten ist, dass der Spaltentrennzeichen auf `\t` für Tab umgestellt wird.

Mit Eingabe der Stadt/PLZ lassen sich nun die jeweiligen Koordinaten ermittel.
Diese werden dann einfach in die Open-meteo URL eingefügt.
Daraufhin wird eine JSON Datei ausgehändigt, woraus dann die benötigten Wetterdaten extrahiert werden können.
Alles leider noch sehr dilettantisch, da ich zur Zeit der Erstellung zum einen Objektorientierte Programmierung noch nicht völlig verstanden habe, zum anderen kaum Erfahrung mit PHP/JSON sammeln konnte.


Hosting
-------

Um von den unzulänglichkeiten des Projekts etwas abzulenken, stand früh fest, dieses Online zu hosten.
Dankenswerterweise bietet Uberspace an die Seiten einen Monat lang kostenlos auszuliefern.
Nach diesem Probemonat, kann man dann wählen, wie viel man Zahlen möchte.
Werden keine Zahlungsdaten angegeben, wird das Benutzerkonto automatisch gelöscht.

Nach erstellung eines Kontos, hinterlegen wir einen SSH-Schlüssel im Webinterface von Uberspace.
Dadurch kann einfach eine Verbindung zum Server aufgebaut werden, um die Daten sicher auszutauschen.
Normalerweise liegen diese Schlüssel auf dem Dateisystem und können somit relativ einfach gestohlen/kompromitiert werden.
Deshalb lege ich jedem die nutzung eines Passwortmanagers ans Herzen.
Ob die Möglickeit, den SSH-Key zu speichern und bei Bedarf an den SSH-Agent zu übergeben, auch bei Anderen Managern möglich ist, weiß ich leider nicht.
Ich kann nur sagen, dass `Keepassxc <https://keepassxc.org/>`_ seit Jahren zuverlässig seinen Dienst bei mir verrichtet.

.. code-block:: console

    # Ordner für Uberspace erstellen
    mkdir -p ~/p_weather/uberspace/html
    mkdir ~/p_weather/uberspace/root

    # entfernte Ordner lokal einbinden
    sshfs wetter@cygnus.uberspace.de:/home/wetter/html/ /home/moe/p_weather/uberspace/html
    sshfs wetter@cygnus.uberspace.de:/home/wetter/ /home/moe/p_weather/uberspace/root

    # Dateien auf Server kopieren
    cp ~/p_weather/index.html ~/p_weather/uberspace/html/

    mkdir ~/p_weather/uberspace/html/php
    cp ~/p_weather/php/weather.php ~/p_weather/uberspace/html/php/

    mkdir ~/p_weather/uberspace/html/css
    cp ~/p_weather/css/style.css ~/p_weather/uberspace/html/css/
    cp ~/p_weather/wetter_daten.sql ~/p_weather/uberspace/root/

    # per SSH mit Server verbinden
    ssh wetter@cygnus.uberspace.de 

    # Datenbank auf Server erstellen
    mysql -e "CREATE DATABASE wetter_db"

    # Dump in Datenbank importieren
    mysql wetter_db < wetter_db.sql








.. raw:: html

   <details>
   <summary><a>Warum der Name?</a></summary>

Because `P <https://www.youtube.com/shorts/SsDyNOB1hZs/>`_ is perfect...

   

.. raw:: html

   </details>




