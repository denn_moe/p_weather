<?php


//           _               
//          | |              
//  ___  ___| |_ _   _ _ __  
// / __|/ _ \ __| | | | '_ \ 
// \__ \  __/ |_| |_| | |_) |
// |___/\___|\__|\__,_| .__/ 
//                    | |    
//                    |_|    

// muss das hier oben?
// jetzt kann ich die nicht mehr ändern...
$input = $_POST['plz'];
$ort = strip_tags($input);
$timestamp = date("Y-m-d H:i:s");
$db_str = '';

$koordinaten = koordinaten_kriegen($ort);
$wetter_array = wetter_abfragen($koordinaten);
$db_temp = pruefe_db($koordinaten, $wetter_array);

if ($db_temp === false) {
    $temp_av = $wetter_array[0];
    $av_str = 'erster eintrag...';
}
else {
    $temp_av = ($db_temp[0] + $wetter_array[0]) / 2;
    $av_str = $temp_av . '°C';
}


// sql vodoo
$db = new PDO('mysql:host=localhost; dbname=wetter_db', 'root', '');
$sql = 'INSERT INTO wetter_daten(plz, name, temp, temp_av, time)VALUES(:plz, :name, :temp, :temp_av, :time)
ON DUPLICATE KEY UPDATE `temp` = :temp, `temp_av` = :temp_av, `time` = :time';

$daten = array(
        'plz' => $koordinaten[3],
        'name' => $koordinaten[2],
        'temp' => $wetter_array[0],
        'temp_av' => $temp_av,
        'time' => $timestamp,
);

$statement = $db->prepare($sql);
$statement->execute($daten);


//  _     _             _ 
// | |   | |           | |
// | |__ | |_ _ __ ___ | |
// | '_ \| __| '_ ` _ \| |
// | | | | |_| | | | | | |
// |_| |_|\__|_| |_| |_|_| 
?>
                       
                       
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wetter</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
    <h1>
        <?php echo $koordinaten[2]; ?>
    </h1>
    <h2>
        aktuell
    </h2>
    <div>
        <?php echo $wetter_array[0]; ?>°C
    </div>
    <h2>
        durchschnitt
    </h2>
    <div>
        <?php echo $av_str; ?>
    </div>
    <h3>
        letztes update 
    </h3>
    <div>
        <?php echo $db_temp[1]; ?>
    </div>
    <h2>
        prognose heute
    </h2>
    <div>
        Minimum <?php echo $wetter_array[3]; ?>°C
    </div>
    <div>
        Maximum <?php echo $wetter_array[4]; ?>°C
    </div>
    <div>
        Regenmenge: <?php echo $wetter_array[1]; ?>mm
    </div>
    <div>
        Regenstunden: <?php echo $wetter_array[2]; ?>
    </div>
    
    <footer>
        <a href="https://codeberg.org/denn_moe/p_weather" target="_blank">git repo</a>
    </footer>
</body>
<?php

//   __                  _   _                 
//  / _|                | | (_)                
// | |_ _   _ _ __   ___| |_ _  ___  _ __  ___ 
// |  _| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
// | | | |_| | | | | (__| |_| | (_) | | | \__ \
// |_|  \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
                                            
                                            
function pruefe_db($koordinaten) {

        // sql vodoo
        $db = new PDO('mysql:host=localhost; dbname=wetter_db', 'root', '');
        $sql = 'SELECT temp, time FROM wetter_daten WHERE plz="'.$koordinaten[3].'"';
        $statement = $db->query($sql);
        $data = $statement->fetchAll();

        // wenn $data leer, dann kein eintrag
        if (isset($data[0])) {
            // echo 'gefunden!!!';
            $db_temp = $data[0][0];
            $db_time = $data[0][1];
            return array($db_temp, $db_time);
        }
        else {
            return false;
        }
}


function wetter_abfragen($koordinaten) {

    // URL erzeugen
    $jsonurl = "https://api.open-meteo.com/v1/dwd-icon?latitude=" . $koordinaten[0] . "&longitude=" . $koordinaten[1] . "&current_weather=true&daily=rain_sum,precipitation_hours&daily=temperature_2m_max,temperature_2m_min&timezone=Europe%2FBerlin";

    // JSON Vodoo
    $json = file_get_contents($jsonurl);
    $weather = json_decode($json, true);
    $temp = $weather['current_weather']['temperature'];
    $regen_menge = $weather['daily']['rain_sum'][0];
    $regen_stunden = $weather['daily']['precipitation_hours'][0];
    $temp_min = $weather['daily']['temperature_2m_min'][0];
    $temp_max = $weather['daily']['temperature_2m_max'][0];
    
    return array($temp,$regen_menge,$regen_stunden,$temp_min,$temp_max);
}

function koordinaten_kriegen($ort){


    // PDO mit Daten füttern
    $db = new PDO('mysql:host=localhost; dbname=wetter_db', 'root', '');

    // wenn Zahl, dann nach PLZ suchen
    if (ctype_digit($ort)) {
        $sql = 'SELECT * FROM `plz` WHERE plz="'.$ort.'"';
    }
    else {
        $sql = 'SELECT * FROM `plz` WHERE ort="'.$ort.'"';
    }

        
    // datenbank Vodoo
    $statement = $db->query($sql);
    $daten = $statement->fetchAll();


    if (isset($daten[0])) {
        // handlichere Koordinaten
        $lat = $daten[0][3];
        $lon = $daten[0][2];
        $stadt_name = $daten[0][4];
        $stadt_plz = $daten[0][1];

        // open-meteo möchte max 4 stellen
        $rnd_lat = round($lat, 4);
        $rnd_lon = round($lon, 4);

        // lat = 0, lon = 1, name = 2, plz = 3
        return array($rnd_lat,$rnd_lon,$stadt_name,$stadt_plz);
    }
    else {
    return array('54.879', '8.3649', 'Westerland', '25980');
    }


}


?>
